# Simple Websocket Feed

## Installation

```shell
$ virtualenv .
$ pip install -r requirements.pip

```

## Usage
```shell
$ source bin/activate
$ python server.py
```

You can then get some of that sweet Web Socket action, for example:

```javascript
var webSocket = new WebSocket('ws://localhost:9000/');
```