import json
import random
from datetime import datetime

from autobahn.twisted.websocket import WebSocketServerProtocol, \
    WebSocketServerFactory
from twisted.internet.task import LoopingCall

import names

LANGUAGE_LIST = ['EN', 'FR']
CARD_TYPE_LIST = ['DELEGATE', 'CUSTOMER_HOST', 'VIP', 'STAFF', 'PRESS']
RFID_CHARS = ['a', 'b', 'c', 'd', 'e', 'f', '0', '1', '2', '3', '4', '5',
              '6', '7', '8', '9']
MASTER_RFID = 'master_rfid'
MESSAGE_FREQUENCY = 30.0


def create_message(counter):
    now = datetime.now()
    guest_obj = {'profile': {'rfid': ''.join([random.choice(RFID_CHARS) for x
                                              in range(10)]),
                             'time': now.strftime('%H:%M'),
                             'first_name': names.get_first_name(),
                             'last_name': names.get_last_name(),
                             'language': random.choice(LANGUAGE_LIST),
                             'card_type': random.choice(CARD_TYPE_LIST),
                             'these': 'a',
                             'fields': 'b',
                             'should': 'c',
                             'not': 'd',
                             'display': 'x'},
                 'time': now.strftime('%H:%M')}

    # every MESSAGE_FREQUENCY send master rfid
    if not (counter % MESSAGE_FREQUENCY):
        guest_obj['rfid'] = MASTER_RFID
    return json.dumps(guest_obj, ensure_ascii=False).encode('utf-8')


class MyServerProtocol(WebSocketServerProtocol):

    def __init__(self):
        self.message_counter = 0
        self._cache = [create_message(x) for x in range(10)]
        self.publisher = LoopingCall(self.send_stuff)

    def send_stuff(self):
        self.message_counter += 1
        msg = create_message(self.message_counter)
        self.sendMessage(msg)
        print("Send message: %s" % msg)
        print(self.message_counter)

    def onConnect(self, request):

        print("Client connecting: {0}".format(request.peer))

    def onOpen(self):
        if self._cache:
            for e in self._cache:
                self.sendMessage(e, False)
            self._cache = []

        print("WebSocket connection open.")
        self.publisher.start(MESSAGE_FREQUENCY)

    def onMessage(self, payload, isBinary):
        if isBinary:
            print("Binary message received: {0} bytes".format(len(payload)))
        else:
            print("Text message received: {0}".format(payload.decode('utf8')))

        # echo back message verbatim
        self.sendMessage(payload, isBinary)

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))
        self.publisher.stop()


if __name__ == '__main__':

    import sys

    from twisted.python import log
    from twisted.internet import reactor

    log.startLogging(sys.stdout)
    factory = WebSocketServerFactory("ws://127.0.0.1:9000", debug=False)
    factory.protocol = MyServerProtocol

    reactor.listenTCP(9000, factory)
    reactor.run()
